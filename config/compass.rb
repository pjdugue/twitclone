require 'compass'

project_type = :rails
http_path    = '/'
sass_dir     = 'app/views/stylesheets'

# File: config/compass.rb
css_dir = "tmp/stylesheets/compiled"
