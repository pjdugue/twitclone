Rails.application.routes.draw do

  get    'sessions/new'

  #index
  root   'users#feed'

  #routes
  get    'signup' => 'users#new'
  get    'login'  => 'sessions#new'
  post   'login'  => 'sessions#create'
  get    'logout' => 'sessions#destroy'

  #ressources
  resources :tweets       , only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :users do
    member do
      get :following, :followers
    end
  end

end
