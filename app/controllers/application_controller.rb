class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include ActionView::Helpers::TextHelper
  include SessionsHelper



  private
   def logged_in_user
     unless logged_in?
       store_location
       flash[:danger] = "you need to be logged in to acces this section"
       redirect_to login_url
     end
   end

end
