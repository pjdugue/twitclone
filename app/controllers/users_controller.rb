class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index,:feed, :show, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user  , only: [:edit, :update]
  before_action :admin_user    , only: :destroy

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user   = User.find(params[:id])
    @tweets = @user.tweets.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)

    if @user.save
      log_in @user
      flash[:success] = "Welcome!!!!"
      redirect_to @user
    else
      render 'new'
    end
  end

  def update

    if @user.update_attributes(user_params)
      flash[:success] = "Update ok"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def following
    @title = "Waves"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  def feed
    if !logged_in?
      redirect_to login_url
    else
      @tweet      = current_user.tweets.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :username, :email, :password, :password_confirmation, :description, :location, :background)
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(@user) unless current_user?(@user)
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end