module ApplicationHelper

  def full_title(page_title = '')
    base_title = "Ritl : just like Twitter in better"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end


  def body_class(class_name="")
    content_for :body_class, class_name
  end

  def build_links_in_tweet(tweet)
    tweet = CGI::escapeHTML(tweet)

    #tweet = build_mentions(tweet)

    mentions = tweet.scan(/ @(\w*[^.,^;,^ ,^#,@]+\w*)/i)

    mentions.each do |mention|
      tweet = tweet.gsub(/@#{mention[0]}/, '<a href="'+mention[0]+'">@'+mention[0]+'</a>')
      end
    #tweet = build_hashtags(tweet)

    tags = tweet.scan(/ #(\w*[^.,^;,^ ,^#,@]+\w*)/i)

    tags.each do |tag|
      tweet = tweet.gsub(/##{tag[0]}/, '<a href="'+tag[0]+'">#'+tag[0]+'</a>')


    end

    return tweet


  end



  def build_mentions(tweet)
    mentions = tweet.scan(/ @(\w*[^.,^;,^ ,^#,@]+\w*)/i)

    mentions.each do |mention|
      tweet = tweet.gsub(/@#{mention[0]}/, '<a href="'+mention[0]+'">@'+mention[0]+'</a>')
    end
  end



  def build_hashtags(tweet)
    tags = tweet.scan(/ #(\w*[^.,^;,^ ,^#,@]+\w*)/i)

    tags.each do |tag|
      tweet = tweet.gsub(/@#{tag[0]}/, '<a href="'+tag[0]+'">#'+tag[0]+'</a>')
    end
  end






end

