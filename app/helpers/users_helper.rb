module UsersHelper

  def format_username(username)
    "@"+username
  end

  def check_background(user)
    if user.background.nil? || user.background ==''
      "https://cldup.com/pPWJGWlf0V-2000x2000.jpeg"
    else
      user.background
    end
  end

  def get_user_description_for_current_user(user)
    if user.description.nil? || user.description ==''
      'your description is blank, you can create it in your settings, and set your background or more. For now, others RITLers see :  "Hello, welcome on my profile you can follow me, I\'m a proud RITLer for '+time_ago_in_words(user.created_at).to_s
    else
      user.description
    end
  end

  def get_user_description_for_users(user)
    if user.description.nil? || user.description ==''
      "Hello, welcome on my profile you can follow me, I'm a proud RITLer for "+time_ago_in_words(user.created_at).to_s
    else
      user.description
    end
  end

  def get_user_location_for_current_user(user)
    if user.location.nil? || user.location ==''
      "Please set your location in your settings"
    else
      user.location
    end
  end

  def get_user_location_for_users(user)
    if user.location.nil? || user.location ==''
      "Earth, Solar system"
    else
      user.location
    end
  end

end
