require_dependency '../validators/url_validator.rb'
class User < ActiveRecord::Base
  attr_accessor :remember_token

  has_secure_password
  has_many :tweets, dependent: :destroy
  has_many :active_relationships, class_name: "Relationship", foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower

  before_save :create_avatar_url, :create_username

  validates :name, presence: true
  validates :username, uniqueness: true, presence: true
  validates :password, length: {minimum: 5}, allow_blank: true
  validates :email, uniqueness: true, presence: true, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :location, length: {maximum: 100}, allow_blank: true
  validates :description, length: {maximum: 1000}, allow_blank: true
  validates :background, format: {with: /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i, :message => "is not a valid img ( jpg|jpeg|png|gif )"}, url: true , allow_blank: true



  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    self.update_attribute(:remember_digest, User.digest(self.remember_token))
  end

  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  # Returns a user's status feed.
  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Tweet.where("user_id IN (#{following_ids}) OR user_id = :user_id", user_id: id)
  end


  # Follows a user.
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end


  private

  def prep_email
    self.email = self.email.strip.downcase if self.email
  end

  def create_avatar_url
    self.avatar_url = "http://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(self.email)}?s=500"
  end

  def create_username
    self.username = username.delete(' ').delete('.').delete(';').delete('#').delete('@').strip.downcase
  end




end
