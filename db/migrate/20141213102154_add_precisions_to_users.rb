class AddPrecisionsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :description, :text
    add_column :users, :background, :text
    add_column :users, :location, :text
  end
end
