require "spec_helper"

RSpec.describe User, :type => :model do

  it "can be saved" do
    user = User.create(name: "Mordecai", email: "m@rshow.com", password:"000000",password_confirmation:"000000", username:"the Sniper")
    user.save!

    found = User.last
    expect(found.name).to eq("Mordecai")
    expect(found.email).to eq("m@rshow.com")
    expect(found.username).to eq("the Sniper")
  end


  it "requires a name, an email a password and an username" do
    user = User.new
    expect(user.valid?).to eq(false)

    user.name = "Rigby"
    expect(user.valid?).to eq(false)

    user.email = "r@rshow.com"
    expect(user.valid?).to eq(false)

    user.password = "000000"
    expect(user.valid?).to eq(false)

    user.username = "the Sniper"
    expect(user.valid?).to eq(true)
  end

  it "requires a somewhat valid email" do
    user = User.new(name: "Mordecai", password:"000000",password_confirmation:"000000", username:"the Sniper")
    expect(user.valid?).to eq(false)

    user.email = "rigby"
    expect(user.valid?).to eq(false)

    user.email = "rigby@rshow"
    expect(user.valid?).to eq(false)

    user.email = "rigby@rshow.com"
    expect(user.valid?).to eq(true)
  end

  it "is impossible to add the same email or username twice" do
    user = User.create(name: "Mordecai", email: "m@rshow.com", password:"000000",password_confirmation:"000000", username:"theSniper")
    expect(user.valid?).to eq(true)

    other_user = User.create(name: "Rolland", email: "m@rshow.com", password:"000000",password_confirmation:"000000", username:"theSniper2")
    expect(other_user.valid?).to eq(false)

    other_user_again = User.create(name: "Brick", email: "o@rshow.com", password:"000000",password_confirmation:"000000", username:"theSniper3")
    expect(other_user_again.valid?).to eq(true)

    other_user_last = User.create(name: "Marcus", email: "p@rshow.com", password:"000000",password_confirmation:"000000", username:"theSniper3")
    expect(other_user_last.valid?).to eq(false)
  end



  it "Have secured password" do
    user = User.create(name: "Mordecai", email: "m@rshow.com", password:"000000",password_confirmation:"000000", username:"the Sniper ")
    user.save!

    found = User.last
    expect(found.password).to_not eq("000000")
  end
end
