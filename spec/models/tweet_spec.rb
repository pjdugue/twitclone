require 'rails_helper'

RSpec.describe Tweet, :type => :model do


  it "a Tweet can't be empty and is limited to 140 characters" do
    user = User.create(name: "Mordecai", email: "m@rshow.com", password:"0000", username:"the Sniper")
    tweet = Tweet.new(users: user)

    expect(tweet.valid?).to eq(false)
    tweet.content = 'testtesttesttesttesttesttest'
    expect(tweet.valid?).to eq(true)
    tweet.content = 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest'
    expect(tweet.valid?).to eq(false)
  end


  it "a Tweet needs to be linked to a users" do
    user = User.create(name: "Mordecai", email: "m@rshow.com", password:"0000", username:"the Sniper")
    tweet = Tweet.new(users: user, content: "mon super tweet")
    expect(tweet.user).to eq(user)
    expect(tweet.user_id).to eq(user.id)

    expect(tweet.valid?).to eq(true)
    tweet.user = nil
    expect(tweet.valid?).to eq(false)
  end




end
